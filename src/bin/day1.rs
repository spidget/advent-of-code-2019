use std::fs::File;
use std::io::{BufRead, BufReader};
use std::process;

fn fuel_required(mass: i32) -> i32 {
    let fuel = (mass / 3) - 2;
    if fuel <= 0 {
        0
    } else {
        fuel + fuel_required(fuel)
    }
}

fn main() {
    let file = File::open("input/day1").unwrap_or_else(|err| {
        println!("Unable to open input file: {}", err);
        process::exit(1);
    });
    let mut total = 0;

    for line in BufReader::new(file).lines() {
        let line = line.unwrap_or_else(|err| {
            println!("Unable to read line: {}", err);
            process::exit(1);
        });
        let mass: i32 = line.parse().unwrap_or_else(|err| {
            println!("Unable parse line to int: {}", err);
            process::exit(1);
        });
        total += fuel_required(mass);
    }

    println!("{}", total);
}

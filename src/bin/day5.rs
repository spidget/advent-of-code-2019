use advent_of_code::int_code;

use std::process;

fn main() {
    let mem = int_code::parse_input("input/day5").unwrap_or_else(|e| {
        println!("Error reading input: {}", e);
        process::exit(1);
    });

    if let Err(err) = int_code::run(mem.clone(), vec![1]) {
        println!("Error running codes: {}", err);
        process::exit(1);
    }

    if let Err(err) = int_code::run(mem, vec![5]) {
        println!("Error running codes: {}", err);
        process::exit(1);
    }
}

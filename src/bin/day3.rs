use std::collections::HashSet;
use std::fs;
use std::hash::{Hash, Hasher};
use std::process;

#[derive(Copy, Clone, Debug, Ord, PartialOrd)]
struct Coord {
    steps: usize,
    x: isize,
    y: isize,
}

impl PartialEq for Coord {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl Eq for Coord {}

impl Hash for Coord {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
    }
}

#[derive(Copy, Clone, Debug)]
enum Move {
    U(usize),
    D(usize),
    L(usize),
    R(usize),
}

impl Move {
    fn new(m: &str) -> Result<Move, String> {
        let mut chars = m.chars();

        match (chars.next(), chars.as_str().parse()) {
            (Some('U'), Ok(d)) => Ok(Move::U(d)),
            (Some('D'), Ok(d)) => Ok(Move::D(d)),
            (Some('L'), Ok(d)) => Ok(Move::L(d)),
            (Some('R'), Ok(d)) => Ok(Move::R(d)),
            _ => Err(format!("Failed to parse move: {}", m)),
        }
    }

    fn walk(&self, path: &mut HashSet<Coord>, mut pos: Coord) -> Coord {
        match self {
            Move::U(d) => {
                for _ in 0..*d {
                    pos.y += 1;
                    pos.steps += 1;
                    path.insert(pos.clone());
                }
            }
            Move::D(d) => {
                for _ in 0..*d {
                    pos.y -= 1;
                    pos.steps += 1;
                    path.insert(pos.clone());
                }
            }
            Move::L(d) => {
                for _ in 0..*d {
                    pos.x -= 1;
                    pos.steps += 1;
                    path.insert(pos.clone());
                }
            }
            Move::R(d) => {
                for _ in 0..*d {
                    pos.x += 1;
                    pos.steps += 1;
                    path.insert(pos.clone());
                }
            }
        }
        pos
    }
}

fn walk_line(line: String) -> Result<HashSet<Coord>, String> {
    let mut pos = Coord {
        steps: 0,
        x: 0,
        y: 0,
    };
    let mut path = HashSet::new();
    path.insert(pos);

    let moves: Result<Vec<Move>, String> = line.split(',').map(|m| Move::new(m)).collect();

    moves.map(|moves| {
        for m in moves {
            pos = m.walk(&mut path, pos);
        }
        path
    })
}

fn get_lines(path: &str) -> Result<(String, String), String> {
    let input = match fs::read_to_string(path) {
        Ok(input) => input,
        Err(err) => return Err(format!("Error reading file '{}': {}", path, err)),
    };

    let mut lines = input.lines();
    match (lines.next(), lines.next()) {
        (Some(line1), Some(line2)) => Ok((line1.to_string(), line2.to_string())),
        _ => Err("File requires two lines".to_string()),
    }
}

fn main() {
    let (line1, line2) = get_lines("input/day3").unwrap_or_else(|err| {
        println!("Unable to parse lines from file: {}", err);
        process::exit(1);
    });

    let path1 = walk_line(line1).unwrap_or_else(|err| {
        println!("Unable to parse path 1: {}", err);
        process::exit(1);
    });
    let path2 = walk_line(line2).unwrap_or_else(|err| {
        println!("Unable to parse path 2: {}", err);
        process::exit(1);
    });

    let closest = path1
        .intersection(&path2)
        .filter(|&p| {
            p != &Coord {
                steps: 0,
                x: 0,
                y: 0,
            }
        })
        .map(|p| {
            let one = path1.get(p).unwrap();
            let two = path2.get(p).unwrap();
            Coord {
                steps: one.steps + two.steps,
                x: p.x,
                y: p.y,
            }
        })
        .min();

    match closest {
        Some(point) => println!("{}, {} = {}", point.x, point.y, point.steps),
        _ => println!("No crossings"),
    };
}

fn main() {
    let result: Vec<u32> = (234208..=765869)
        .filter(|&x| {
            let mut remaining = x;

            let mut last = remaining % 10;
            let mut dupe_digit = 0;
            let mut found_double = false;

            while remaining > 10 {
                remaining /= 10;
                let cur = remaining % 10;

                if last < cur {
                    return false;
                }
                if last == cur {
                    dupe_digit += 1;
                } else {
                    if dupe_digit == 1 {
                        found_double = true;
                    }
                    dupe_digit = 0;
                }

                last = cur;
            }
            found_double || dupe_digit == 1
        })
        .collect();
    println!("{:?}", result.len());
}

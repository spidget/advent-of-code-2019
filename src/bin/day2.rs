use advent_of_code::int_code;

use std::process;

fn main() {
    let mem = int_code::parse_input("input/day2").unwrap_or_else(|e| {
        println!("Error reading input: {}", e);
        process::exit(1);
    });

    let mut noun = 0;
    let mut verb = 0;
    loop {
        let mut input = mem.clone();
        input[1] = noun;
        input[2] = verb;

        match int_code::run(input, vec![]) {
            Ok(19690720) => {
                println!("{}", 100 * noun + verb);
                return;
            }
            Err(err) => {
                println!("Error running codes: {}", err);
                process::exit(1);
            }
            _ => {}
        }
        noun += 1;
        if noun == 100 {
            noun = 0;
            verb += 1;
        }
    }
}

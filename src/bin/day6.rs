use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::process;

fn count_orbits<'a>(orbits: &'a HashMap<String, String>, object: &str, steps: &mut Vec<&'a str>) {
    if let Some(orbit) = orbits.get(object) {
        steps.push(orbit);
        count_orbits(orbits, orbit, steps);
    };
}

fn main() {
    let file = File::open("input/day6").unwrap_or_else(|err| {
        println!("Unable to open input file: {}", err);
        process::exit(1);
    });

    let mut orbits: HashMap<String, String> = HashMap::new();

    for line in BufReader::new(file).lines() {
        let line = match line {
            Ok(line) => line,
            Err(err) => {
                println!("Error reading line: {}", err);
                process::exit(1);
            }
        };

        let (orbiting, body) = match line.split(')').collect::<Vec<&str>>().as_slice() {
            [orbiting, body] => (orbiting.to_string(), body.to_string()),
            _ => {
                println!("Error parsing orbit from line: {}", line);
                process::exit(1);
            }
        };

        match orbits.get_mut(&body) {
            None => {
                orbits.insert(body, orbiting);
            }
            _ => {
                println!("Object with more than one direct orbit: {}", line);
                process::exit(1);
            }
        }
    }

    let mut santa_steps = vec![];
    count_orbits(&orbits, "SAN", &mut santa_steps);
    let mut my_steps = vec![];
    count_orbits(&orbits, "YOU", &mut my_steps);

    let count = walk(&my_steps, &santa_steps) + walk(&santa_steps, &my_steps);
    println!("{}", count);
}

fn walk(one: &Vec<&str>, two: &Vec<&str>) -> usize {
    let mut uniq = 0;
    for step in one {
        if !two.contains(&step) {
            uniq += 1;
        } else {
            break;
        }
    }
    return uniq;
}

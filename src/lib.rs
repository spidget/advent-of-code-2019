pub mod int_code {
    use std::fs;

    #[derive(Debug)]
    enum Parameter {
        Immediate(isize),
        Position(usize),
    }

    impl Parameter {
        fn get(self, mem: &Vec<isize>) -> isize {
            match self {
                Parameter::Immediate(p) => p,
                Parameter::Position(p) => mem[p as usize],
            }
        }

        fn new(mut opcode: isize, position: usize, parameter: &isize) -> Parameter {
            opcode = opcode / 100;
            opcode = opcode / 10_isize.pow((position - 1) as u32);
            match opcode % 10 {
                0 => Parameter::Position(*parameter as usize),
                _ => Parameter::Immediate(*parameter),
            }
        }
    }

    enum OpCode {
        Add {
            op1: Parameter,
            op2: Parameter,
            dest: usize,
        },
        Multiply {
            op1: Parameter,
            op2: Parameter,
            dest: usize,
        },
        Save {
            dest: usize,
        },
        Output {
            src: Parameter,
        },
        JumpIfTrue {
            chk: Parameter,
            to: Parameter,
        },
        JumpIfFalse {
            chk: Parameter,
            to: Parameter,
        },
        LessThan {
            op1: Parameter,
            op2: Parameter,
            dest: usize,
        },
        Equals {
            op1: Parameter,
            op2: Parameter,
            dest: usize,
        },
        Finish,
    }

    impl OpCode {
        fn new(mem: &Vec<isize>, ip: usize) -> Result<OpCode, String> {
            let code = match mem.get(ip) {
                Some(c) => c,
                _ => return Err("Reading beyond memory".to_string()),
            };

            match code % 100 {
                1 => match (mem.get(ip + 1), mem.get(ip + 2), mem.get(ip + 3)) {
                    (Some(op1), Some(op2), Some(dest)) => Ok(OpCode::Add {
                        op1: Parameter::new(*code, 1, op1),
                        op2: Parameter::new(*code, 2, op2),
                        dest: *dest as usize,
                    }),
                    _ => Err("Malformed Input for Add".to_string()),
                },
                2 => match (mem.get(ip + 1), mem.get(ip + 2), mem.get(ip + 3)) {
                    (Some(op1), Some(op2), Some(dest)) => Ok(OpCode::Multiply {
                        op1: Parameter::new(*code, 1, op1),
                        op2: Parameter::new(*code, 2, op2),
                        dest: *dest as usize,
                    }),
                    _ => Err("Malformed input for Multiply".to_string()),
                },
                3 => match mem.get(ip + 1) {
                    Some(dest) => Ok(OpCode::Save {
                        dest: *dest as usize,
                    }),
                    _ => Err("Malformed input for Save".to_string()),
                },
                4 => match mem.get(ip + 1) {
                    Some(src) => Ok(OpCode::Output {
                        src: Parameter::new(*code, 1, src),
                    }),
                    _ => Err("Malformed input for Output".to_string()),
                },
                5 => match (mem.get(ip + 1), mem.get(ip + 2)) {
                    (Some(chk), Some(to)) => Ok(OpCode::JumpIfTrue {
                        chk: Parameter::new(*code, 1, chk),
                        to: Parameter::new(*code, 2, to),
                    }),
                    _ => Err("Malformed input for JumpIfTrue".to_string()),
                },
                6 => match (mem.get(ip + 1), mem.get(ip + 2)) {
                    (Some(chk), Some(to)) => Ok(OpCode::JumpIfFalse {
                        chk: Parameter::new(*code, 1, chk),
                        to: Parameter::new(*code, 2, to),
                    }),
                    _ => Err("Malformed input for JumpIfFalse".to_string()),
                },
                7 => match (mem.get(ip + 1), mem.get(ip + 2), mem.get(ip + 3)) {
                    (Some(op1), Some(op2), Some(dest)) => Ok(OpCode::LessThan {
                        op1: Parameter::new(*code, 1, op1),
                        op2: Parameter::new(*code, 2, op2),
                        dest: *dest as usize,
                    }),
                    _ => Err("Malformed input for LessThan".to_string()),
                },
                8 => match (mem.get(ip + 1), mem.get(ip + 2), mem.get(ip + 3)) {
                    (Some(op1), Some(op2), Some(dest)) => Ok(OpCode::Equals {
                        op1: Parameter::new(*code, 1, op1),
                        op2: Parameter::new(*code, 2, op2),
                        dest: *dest as usize,
                    }),
                    _ => Err("Malformed input for Equals".to_string()),
                },
                99 => Ok(OpCode::Finish),
                c => Err(format!("Invalid opcode: {}", c)),
            }
        }
    }

    pub fn parse_input(path: &str) -> Result<Vec<isize>, String> {
        let input = match fs::read_to_string(path) {
            Ok(s) => s,
            Err(e) => return Err(format!("Unable to read input: {}", e)),
        };

        input
            .trim_end()
            .split(',')
            .map(|i| {
                i.parse()
                    .map_err(|e| format!("Unable to parse int ({}): {}", i, e))
            })
            .collect()
    }

    pub fn run(mut mem: Vec<isize>, mut input: Vec<isize>) -> Result<isize, String> {
        let mut ip = 0;
        loop {
            match OpCode::new(&mem, ip) {
                Ok(OpCode::Add { op1, op2, dest }) => {
                    mem[dest] = op1.get(&mem) + op2.get(&mem);
                    ip += 4;
                }
                Ok(OpCode::Multiply { op1, op2, dest }) => {
                    mem[dest] = op1.get(&mem) * op2.get(&mem);
                    ip += 4;
                }
                Ok(OpCode::Save { dest }) => {
                    match input.pop() {
                        Some(i) => {
                            mem[dest] = i;
                            ip += 2;
                        }
                        None => return Err("Not enough input".to_string()),
                    };
                }
                Ok(OpCode::Output { src }) => {
                    println!("{:?}", src.get(&mem));
                    ip += 2;
                }
                Ok(OpCode::JumpIfTrue { chk, to }) => {
                    ip = if chk.get(&mem) != 0 {
                        to.get(&mem) as usize
                    } else {
                        ip + 3
                    };
                }
                Ok(OpCode::JumpIfFalse { chk, to }) => {
                    ip = if chk.get(&mem) == 0 {
                        to.get(&mem) as usize
                    } else {
                        ip + 3
                    };
                }
                Ok(OpCode::LessThan { op1, op2, dest }) => {
                    mem[dest] = if op1.get(&mem) < op2.get(&mem) { 1 } else { 0 };
                    ip += 4;
                }
                Ok(OpCode::Equals { op1, op2, dest }) => {
                    mem[dest] = if op1.get(&mem) == op2.get(&mem) { 1 } else { 0 };
                    ip += 4;
                }
                Ok(OpCode::Finish) => return Ok(mem[0]),
                Err(err) => return Err(err),
            }
        }
    }
}
